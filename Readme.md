# Europe One Degree Warmer - Data

This repository contains the data, downloaded from ECMWF, used in the [Europe, One Degree Warmer](https://www.onedegreewarmer.eu/) project, as well as the script used to create the averages and other computations for each location in the analysis.

Each location contains two files, one for the "interim" database and one for the "era-20c" database of ECMWF. The script used to reconcile both datasets is available [as a Jupyter Notebook here](https://nbviewer.jupyter.org/urls/pastebin.com/raw/DGQSCRbY#Merging-the-ECMWF-data-sets).