#!/usr/bin/env python3

import pandas as pd
import numpy as np
from csv import DictReader
import json
import math
from modules.climatezones import Koeppen


def getDistance(r, lat1, lon1):
    lat2 = r["lat"]
    lon2 = r["lon"]

    radius = 6367.47  # km

    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(lon2 - lon1)
    a = math.sin(dlat / 2) * math.sin(dlat / 2) \
        + math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) \
        * math.sin(dlon / 2) * math.sin(dlon / 2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = radius * c

    return math.floor(d)


# Cities from the first batch
cities_df = pd.read_csv("cities_deduplicated_with_temp.csv")
# Cities from successive data collection batches
additional_cities_df = pd.read_csv("additionnal_cities_list.csv")
# Combines the two dataframes
cities_df = cities_df.merge(additional_cities_df,
                            on=["city_name", "country_code"],
                            how="outer")

koeppen = Koeppen()


# Computes the temperature_diff for new cities
def computeTempDiff(row):
    if row["temperature increase"] > 0:
        return round(row["temperature increase"],4)
    else:
        city_name = row["city_name"]
        city_csv_era20c_filename = ".city_data/%s/1900-1997-era20c-corrected.csv" % city_name
        city_csv_interim_filename = "city_data/%s/1979-2018-interim.csv" % city_name
        try:
            city_era20c_df = pd.read_csv(city_csv_era20c_filename, index_col="date", parse_dates=True)
            city_interim_df = pd.read_csv(city_csv_interim_filename, index_col="date", parse_dates=True)
        except FileNotFoundError:
            return np.nan
        city_df = city_era20c_df.loc[(city_era20c_df.index.year < 1979)].append(city_interim_df)
        city_df.loc[city_df["value"] < -50, 'value'] = np.nan
        city_df.loc[city_df["value"] > 60, 'value'] = np.nan
        # Resamples to include Feb 29
        city_df = city_df.resample('D').mean()
        # Fills NaN values
        city_df = city_df.interpolate()
        city_by_year_20c = city_df.groupby(city_df.index.year).mean()
        temp_20c = city_by_year_20c.loc[city_by_year_20c.index < 2000]["value"].mean()
        city_by_year_21c = city_df.groupby(city_df.index.year).mean()
        temp_21c = city_by_year_21c.loc[city_by_year_21c.index >= 2000]["value"].mean()
        return round(temp_21c - temp_20c, 4)


cities_df["temperature increase"] = cities_df.apply(computeTempDiff, axis=1)

# Fixes lat and lon
cities_df.rename(columns={'lat_y': 'lat', 'lon_y': 'lon'}, inplace=True)

# Sorts df
cities_df = cities_df.sort_values(by='temperature increase', ascending=False)

# Associates cities and squares
squares_to_cities = {}
cities_to_squares = {}
with open("cities_and_squares.csv", "r") as file:
    reader = DictReader(file)
    for row in reader:
        cities_to_squares[row["name"]] = row["DN"]
        if row["DN"] not in squares_to_cities:
            squares_to_cities[row["DN"]] = []
        squares_to_cities[row["DN"]].append(row["name"])

# Add city sizes
city_sizes_df = pd.read_csv("city_sizes.csv")
cities_df = pd.merge(cities_df, city_sizes_df, on="city_name")

# Add major EU cities-list
big_cities_df = cities_df.loc[cities_df["size"].isin(["bigger", "biggest"])]
big_cities_df = big_cities_df.sort_values(by='temperature increase', ascending=False)
number_of_big_cities = len(big_cities_df)
big_city_list = []
for city_big, row_big in big_cities_df.iterrows():
    big_city_list.append({
        "city_name": row_big["city_name"],
        "temperature_diff": row_big["temperature increase"],
        "country": row_big["country_code"],
        "rank": len(big_cities_df.loc[big_cities_df["temperature increase"] >= row_big["temperature increase"]])
    })

total_json = {}
for city, row in cities_df.iterrows():
    city_code = row["city_code"]
    city_name = row["city_name"]
    # only take cities with valid data
    if np.isnan(row["temperature increase"]):
        continue

    # first batch
    elif row["population"] > 0:
        city_csv_era20c_filename = "city_data/%s/1900-1997-era20c-corrected.csv" % city_code
        city_csv_interim_filename = "city_data/%s/1979-2018-interim.csv" % city_code

        city_era20c_df = pd.read_csv(city_csv_era20c_filename, index_col="Date", usecols=["Date", "2 metre temperature"], parse_dates=True)
        city_interim_df = pd.read_csv(city_csv_interim_filename, index_col="Date", usecols=["Date", "2 metre temperature"], parse_dates=True)
    # subsequent batches
    else:
        city_csv_era20c_filename = "../Data_Acquisition/data/%s_era20c.csv" % city_name
        city_csv_interim_filename = "../Data_Acquisition/data/%s_interim.csv" % city_name
        city_era20c_df = pd.read_csv(city_csv_era20c_filename, index_col="date", parse_dates=True)
        city_interim_df = pd.read_csv(city_csv_interim_filename, index_col="date", parse_dates=True)
        city_era20c_df.rename(columns={'value': '2 metre temperature'}, inplace=True)
        city_interim_df.rename(columns={'value': '2 metre temperature'}, inplace=True)

    city_df = city_era20c_df.loc[(city_era20c_df.index.year < 1979)].append(city_interim_df)

    # Removes extreme bogus values
    city_df.loc[city_df["2 metre temperature"] < -50, '2 metre temperature'] = np.nan
    city_df.loc[city_df["2 metre temperature"] > 60, '2 metre temperature'] = np.nan

    # Resamples to include Feb 29
    city_df = city_df.resample('D').mean()

    # Fills NaN values
    city_df = city_df.interpolate()

    city_by_year = city_df.groupby(city_df.index.year).mean()

    # Defines hot days
    hot_temperature = int(city_df["2 metre temperature"].mean() + 2 * city_df["2 metre temperature"].std())

    # Five warmest years
    warmest_years = city_by_year.sort_values(by=["2 metre temperature"], ascending=False)
    warmest_years = warmest_years.head(5).index.tolist()

    # Temperature difference, average temp in 20 and 21st centuries
    avg_20th_cent = city_by_year.loc[city_by_year.index < 2000].mean()["2 metre temperature"]
    avg_21st_cent = city_by_year.loc[city_by_year.index >= 2000].mean()["2 metre temperature"]

    temperature_diff = round(avg_21st_cent - avg_20th_cent, 4)

    # Hot days
    city_hot_days = city_df.loc[(city_df["2 metre temperature"] > hot_temperature)]
    city_hot_days_per_year = city_hot_days.groupby(city_hot_days.index.year).count().reindex(np.arange(1900, 2018)).fillna(0)

    hot_days_avg_20th_cent = city_hot_days_per_year.loc[city_hot_days_per_year.index < 2000].mean()["2 metre temperature"]
    hot_days_avg_21st_cent = city_hot_days_per_year.loc[city_hot_days_per_year.index >= 2000].mean()["2 metre temperature"]

    # Computes freezing days
    city_freeze_days = city_df.loc[(city_df["2 metre temperature"] < -1)]
    city_freeze_days_per_year = city_freeze_days.groupby(city_freeze_days.index.year).count().reindex(np.arange(1900, 2018)).fillna(0)

    freeze_days_avg_20th_cent = city_freeze_days_per_year.loc[city_freeze_days_per_year.index < 2000].mean()["2 metre temperature"]
    freeze_days_avg_21st_cent = city_freeze_days_per_year.loc[city_freeze_days_per_year.index >= 2000].mean()["2 metre temperature"]

    # School days above 22C
    school_calendar = {
        "FR": {"start": [9, 1], "end": [7, 8]},
        # http://www.education.gouv.fr/cid197/les-archives-calendrier-scolaire-partir-1960.html
        "BE": {"start": [9, 1], "end": [7, 1]},
        # http://www.enseignement.be/index.php?page=23953
        "IT": {"start": [9, 5], "end": [6, 16]},
        # Worst case scenario https://www.skuola.net/ritorno-scuola/calendario-scolastico.html
        "ES": {"start": [9, 6], "end": [6, 22]},
        # https://www.elconfidencial.com/espana/madrid/2016-06-29/calendario-escolar-curso2016-2017-comunidad-madrid_1225608/
        "SE": {"start": [8, 25], "end": [6, 10]},
        "FI": {"start": [8, 15], "end": [6, 5]},
        "NO": {"start": [8, 20], "end": [6, 20]},
        "DK": {"start": [8, 15], "end": [6, 20]},
        "EE": {"start": [9, 1], "end": [6, 10]},
        "LV": {"start": [9, 1], "end": [5, 31]},
        "LT": {"start": [9, 1], "end": [5, 31]},
        "IS": {"start": [8, 24], "end": [6, 10]},
        "SI": {"start": [9, 1], "end": [6, 24]},
        "default": {"start": [9, 1], "end": [7, 1]}
    }
    # Counts days above 26°C
    city_school_hot_days = city_df.loc[(city_df["2 metre temperature"] >= 22)]

    # Restricts to school year
    country = row["country_code"]
    if country in school_calendar:
        start_month = school_calendar[country]["start"][0]
        start_day = school_calendar[country]["start"][1]
        end_month = school_calendar[country]["end"][0]
        end_day = school_calendar[country]["end"][1]
    else:
        start_month = school_calendar["default"]["start"][0]
        start_day = school_calendar["default"]["start"][1]
        end_month = school_calendar["default"]["end"][0]
        end_day = school_calendar["default"]["end"][1]
    city_school_hot_days = city_school_hot_days.loc[
        (city_school_hot_days.index.month > start_month |
        ((city_school_hot_days.index.month >= start_month) & (city_school_hot_days.index.day >= start_day))) |
        (city_school_hot_days.index.month < end_month |
        ((city_school_hot_days.index.month <= end_month) & (city_school_hot_days.index.day <= end_day))) &
        (city_school_hot_days.index.weekday < 5)
    ]

    city_school_hot_days_per_year = city_school_hot_days.groupby(city_school_hot_days.index.year).count().reindex(np.arange(1900, 2018)).fillna(0)

    school_hot_days_avg_20th_cent = city_school_hot_days_per_year.loc[city_school_hot_days_per_year.index < 2000].mean()["2 metre temperature"]
    school_hot_days_avg_21th_cent = city_school_hot_days_per_year.loc[city_school_hot_days_per_year.index >= 2000].mean()["2 metre temperature"]

    # Change category
    if temperature_diff > 1.3:
        change = "fast"
    elif temperature_diff < 0.6:
        change = "slow"
    else:
        change = "medium"

    # Number of cities
    number_of_cities = len(cities_df)

    # city rank
    rank = len(cities_df.loc[cities_df["temperature increase"] >= temperature_diff])
    country_rank = len(cities_df.loc[(cities_df["temperature increase"] >= temperature_diff) & (cities_df["country_code"] == country)])

    # Major cities rank
    city_is_big = False
    big_city_rank = None
    if row["size"] in ["bigger", "biggest"]:
        city_is_big = True
        big_city_rank = len(big_cities_df.loc[cities_df["temperature increase"] >= temperature_diff])

    # country list
    country_df = cities_df.loc[cities_df["country_code"] == country]

    if country == "SE":
        # Special case: Include Copenhagen as Malmö in Swedish top list
        malmo = cities_df[cities_df["city_name"] == "København"]
        malmo.city_name = "Malmö"
        country_df = country_df.append(malmo)

    number_of_cities_country = len(country_df)
    country_list = []
    for city_country, row_country in country_df.iterrows():
        country_list.append({
            'city_name': row_country["city_name"],
            'temperature_diff': row_country["temperature increase"],
            'rank': len(cities_df.loc[cities_df["temperature increase"] >= row_country["temperature increase"]])})

    country_top = {"city_name": country_df.head(1).iloc[0]["city_name"]
                 , "temperature_diff": country_df.head(1).iloc[0]["temperature increase"]}

    country_bottom = {"city_name": country_df.tail(1).iloc[0]["city_name"]
                 , "temperature_diff": country_df.tail(1).iloc[0]["temperature increase"]}

    # Nearby cities
    nearby = []

    cities_df["distance"] = cities_df.apply(getDistance, args=(row["lat"],row["lon"]), axis=1)
    nearby_df = cities_df.sort_values(by="distance").iloc[1:6] # uses iloc and not head because first city is current city

    for city_nearby, row_nearby in nearby_df.iterrows():
        nearby.append({"city_name": row_nearby["city_name"]
                      , "country": row_nearby["country_code"]
                      , "temperature_diff": row_nearby["temperature increase"]
                      , "rank": len(cities_df.loc[cities_df["temperature increase"] >= row_nearby["temperature increase"]])
                      , "distance": row_nearby["distance"]})

    # Climate zone
    zone = koeppen.get_zone((row["lon"], row["lat"]))
    if zone:
        zone = zone.description
    else:
        zone = ""

    # Stores for the JSON
    if pd.isnull(city_code):
        city_code = None
    values_for_report = {
        "climate_zone": zone,
        "city_code": city_code,
        "city_name": city_name,
        "hot_temperature": hot_temperature,
        "warmest_years": warmest_years,
        "temperature_diff": round(temperature_diff, 4),
        "avg_20th_cent": round(avg_20th_cent, 3),
        "avg_21st_cent": round(avg_21st_cent, 3),
        "hot_days_avg_20th_cent": round(hot_days_avg_20th_cent, 3),
        "hot_days_avg_21st_cent": round(hot_days_avg_21st_cent, 3),
        "freeze_days_avg_20th_cent": round(freeze_days_avg_20th_cent, 3),
        "freeze_days_avg_21st_cent": round(freeze_days_avg_21st_cent, 3),
        "school_hot_days_avg_20th_cent": round(school_hot_days_avg_20th_cent, 3),
        "school_hot_days_avg_21th_cent": round(school_hot_days_avg_21th_cent, 3),
        "chartdata": {
            "temperature": {
                'data': [("{}-01-01".format(k), round(v, 3) or "") for k, v in city_by_year["2 metre temperature"].to_dict().items()],
                'type': "line",
                'unit': "degrees",
                'title': "temperature_chart_title",
                'caption': "chart_source",
            },
            "freeze_days": {
                'data': [("{}-01-01".format(k), v) for k, v in city_freeze_days_per_year["2 metre temperature"].to_dict().items()],
                'type': "bars",
                'unit': "count",
                'title': "freeze_days_chart_title",
                'caption': "chart_source",
            },
            "hot_days": {
                'data': [("{}-01-01".format(k), v) for k, v in city_hot_days_per_year["2 metre temperature"].to_dict().items()],
                'type': "bars",
                'unit': "count",
                'title': "hot_days_chart_title",
                'caption': "chart_source",
            }
        },
        "country": country,
        "coords": {
            "lat": row["lat"],
            "lon": row["lon"],
        },
        "square_id": cities_to_squares[city_name],
        "cities_in_square": squares_to_cities[cities_to_squares[city_name]],
        "change_rate": change,
        "rank": rank,
        "country_rank": country_rank,
        "number_of_cities": number_of_cities,
        "number_of_cities_country": number_of_cities_country,
        "country_list": country_list,
        "nearby": nearby,
        "country_top": country_top,
        "country_bottom": country_bottom,
        "city_is_big": city_is_big,
        "big_city_rank": big_city_rank,
        "size": row["size"] if type(row["size"]) == str else "",
    }

    # Corrects NaN values to 0
    for key, value in values_for_report.items():
        if type(value) == np.float64:
            values_for_report[key] = np.nan_to_num(values_for_report[key])

    print(city_name, temperature_diff)
    total_json[city_name] = values_for_report

# Copy SE top list to Copenhagen
total_json["København"]["country_list_X"] = total_json["Stockholm"]["country_list"]

top20_df = cities_df.head(20)
bottom_df = cities_df.tail(1)
top20 = []
for city_top20, row_top20 in top20_df.iterrows():
    top20.append({
        "city_name": row_top20["city_name"],
        "country": row_top20["country_code"],
        "temperature_diff": row_top20["temperature increase"],
        "rank": len(cities_df.loc[cities_df["temperature increase"] >= row_top20["temperature increase"]])
    })

total_json["common"] = {
    "big_cities_top20": big_city_list[:20],
    "number_of_big_cities": number_of_big_cities,
    "top20": top20,
    "bottom": {
        "city_name": bottom_df.iloc[0]["city_name"],
        "country": bottom_df.iloc[0]["country_code"],
        "temperature_diff": bottom_df.iloc[0]["temperature increase"]
    },
    "startyear": 1900,
    "endyear": 2017,
    "numyears": 2017 - 1900,
}

json_filename = "cities.json"


def default(o):
    # Convert all int64 to int
    if isinstance(o, np.int64):
        return int(o)
    raise TypeError


with open(json_filename, 'w') as outfile:
    json.dump(total_json, outfile, default=default)
