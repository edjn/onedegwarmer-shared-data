""" This file contains classes for working with climate zones.
It requires a number of data files in the `./data/` directory.

Note that geoanalysis in Python is slow. For anything but very basic use cases,
you probably want to use e.g. C++ with CGAL

By Leo Wallentin, J++ for EDJNet
"""
from csv import DictReader
import os
from shapely.geometry import shape, Point
from six import string_types
from collections import Iterable
import fiona


def point_from_coordinates(input_):
    """ Factory method for Points """
    if isinstance(input_, Point):
        return input_

    if isinstance(input_, tuple):
        t = input_

    elif isinstance(input_, string_types):
        for sep in [",", ";", "|"]:
            if len(input_.split(sep)) in range(2, 3):
                t = input_.split(sep)

    elif isinstance(input_, Iterable):
        # Strings are iterable in Python 3,
        # so this must come after string check
        t = (input_)

    else:
        raise Exception("Invalid coordinates: %s" % input_)

    return Point(tuple(float(x) for x in t))


def point_in_polygon(point, polygon):
    """ Check if a point is in a polygon """
    pnt = point_from_coordinates(point)
    polygon = shape(polygon['geometry'])
    return polygon.contains(pnt)


class InvalidZoneId(Exception):
    """This is not a valid zone id under the current zone system"""
    pass


class ClimateZone(object):
    """Represents a generic climate zone. Can be initiated with the name
    of a CSV file with a translation table. A columns named "id" will be used
    for keys if found, otherwise the first column is assumed to be the key.
    """
    translations = None

    def __init__(self, id, translations=None):
        if translations:
            self.translations = {}
            with open(translations, "r") as file_:
                reader = DictReader(file_)
                for row in reader:
                    try:
                        key = row["id"]
                    except KeyError:
                        key = next(iter(row))
                    if key:
                        # Ignore empty keys and by extension missing rows
                        self.translations[key] = row
        self.id = id

    def __str__(self):
        return self.id

    def __repr__(self):
        return "<{}: {}>".format(type(self).__name__, self.id)


class KoeppenZone(ClimateZone):
    """Represents a Köppen climate zone"""

    gridcodeDict = {
        '11': "Af",
        '12': "Am",
        '13': "As",
        '14': "Aw",
        '21': "BWk",
        '22': "BWh",
        '26': "BSk",
        '27': "BSh",
        '31': "Cfa",
        '32': "Cfb",
        '33': "Cfc",
        '34': "Csa",
        '35': "Csb",
        '36': "Csc",
        '37': "Cwa",
        '38': "Cwb",
        '39': "Cwc",
        '41': "Dfa",
        '42': "Dfb",
        '43': "Dfc",
        '44': "Dfd",
        '45': "Dsa",
        '46': "Dsb",
        '47': "Dsc",
        '48': "Dsd",
        '49': "Dwa",
        '50': "Dwb",
        '51': "Dwc",
        '52': "Dwd",
        '61': "EF",
        '62': "ET",
    }

    def __init__(self, id, translations=None):
        """ Can be initiated with GridCode or Köppen ID,
        eg zone = KoeppenZone(11) or zone = KoeppenZone("Dfa")
        """
        id = str(id)
        if id in self.gridcodeDict:
            id = self.gridcodeDict[id]
        elif id in self.gridcodeDict.values():
            pass
        else:
            raise InvalidZoneId("{} is not a valid Köppen zone".format(id))
        super().__init__(id, translations=translations)

    @property
    def primary(self):
        key = self.id[0]
        return {
            'A': "Tropical",
            'B': "Arid",
            'C': "Temperate",
            'D': "Cold (continental)",
            'E': "Polar",
        }[key]

    @property
    def secondary(self):
        p_key = self.id[0]
        s_key = self.id[1]
        return {
            'A': {
                'f': "Rainforest",
                'm': "Monsoon",
                'w': "Savanna, Wet",
                's': "Savanna, Dry",
            },
            'B': {
                'W': "Desert",
                'S': "Steppe",
            },
            'C': {
                's': "Dry summer",
                'w': "Dry winter",
                'f': "Without dry season",
            },
            'D': {
                's': "Dry summer",
                'w': "Dry winter",
                'f': "Without dry season",
            },
            'E': {
                'T': "Tundra",
                'F': "Eternal winter (ice cap)",
            },
        }[p_key][s_key]

    @property
    def tertiary(self):
        try:
            t_key = self.id[2]
        except IndexError:
            return None
        p_key = self.id[0]
        return {
            'B': {
                'h': "Hot",
                'k': "Cold",
                'n': "With frequent fog",
            },
            'C': {
                'a': "Hot summer",
                'b': "Warm summer",
                'c': "Cold summer",
            },
            'D': {
                'a': "Hot summer",
                'b': "Warm summer",
                'c': "Cold summer",
                'd': "Very cold winter",
            },
        }[p_key][t_key]

    @property
    def description(self):
        """Return an English description for this zone, inteded for use in a
        general text. Multiple zones may share the same description.
        """
        if self.translations:
            return self.translations[self.id]["description"]
        else:
            return self.id

    @property
    def wikidata(self):
        """ Return the corresponding wikidata ID for this zone """
        if self.translations:
            return self.translations[self.id]["wikidata"]
        else:
            return None


class Koeppen(object):
    """This class handles Köppen climate classification data."""

    polygons = []

    def __init__(self):
        cdir = os.path.dirname(os.path.realpath(__file__))
        self.translations_file = os.path.join(cdir, "data",
                                              "koeppen_translations.csv")
        for feat in fiona.open(os.path.join(cdir, "data", "koeppen.shp")):
            self.polygons.append(feat)

    def in_which(self, coordinate):
        """ Find out in wich polygon a coordinate is"""
        for feature in self.polygons:
            if point_in_polygon(coordinate, feature):
                return feature["properties"]["GRIDCODE"]
        return None

    def get_zone(self, coordinates):
        """ Get a zone Object for a coordinate """
        polygon = self.in_which(coordinates)
        if polygon is None:
            return None
        return KoeppenZone(polygon, translations=self.translations_file)


if __name__ == "__main__":
    print("This module is only intended to be called from other scripts.")
    import sys
    sys.exit()
